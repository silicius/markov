package main

import (
	"fmt"
	"strings"
)

type multiStringFlag struct {
	argv []string
}

func newMultiStringFlag() multiStringFlag {
	return multiStringFlag{
		make([]string, 0),
	}
}

func (mf *multiStringFlag) String() string {
	return fmt.Sprint(*mf)
}

func (mf *multiStringFlag) Set(s string) error {
	args := strings.Split(s, ",")
	for _, arg := range args {
		mf.argv = append(mf.argv, arg)
	}
	return nil
}
