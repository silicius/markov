package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"regexp"

	xoroshiro "github.com/dgryski/go-xoroshiro"
)

func main() {
	var options struct {
		// generating output
		seed         int64
		outputLength uint64

		// building model
		inputFilenames multiStringFlag
		tokensInState  uint
		tokenRegex     string

		// loading model
		loadFilenames multiStringFlag

		// saving model
		saveFilename string
		saveIndent   string
		savePrefix   string
	}
	var mainModel = newModel()

	// ------------------------- setup flags; TODO: use something more sane for flag handling
	// generating output
	flag.Int64Var(&options.seed, "seed", 2137, "")
	flag.Uint64Var(&options.outputLength, "outputLength", 0, "How many tokens will be generated from the model.")

	// building model
	flag.Var(&options.inputFilenames, "read", "Files that will be used to build the model.")
	flag.UintVar(&options.tokensInState, "stateLength", 1, "Number of tokens in each state. Must be bigger than 0.")
	flag.StringVar(&options.tokenRegex, "splitRegex", ".{1}", "Regex used to split the input files into tokens.")

	// loading model
	flag.Var(&options.loadFilenames, "load", "Will load the saved model states.")

	// saving model
	flag.StringVar(&options.saveFilename, "save", "modelSave", "Will save the model and some additional metadata using json encoding to a file under the given filename.")
	// TODO: make it an option:
	options.savePrefix = ""
	options.saveIndent = "\t"

	flag.Parse()

	// decide what actions to do based on used flags
	var generate, read, save, load bool
	flag.Visit(
		func(flag *flag.Flag) {
			switch flag.Name {
			case "seed":
				fallthrough
			case "outputLength":
				generate = true

			case "stateLength":
				fallthrough
			case "splitRegex":
				fallthrough
			case "read":
				read = true

			case "save":
				save = true

			case "load":
				load = true
			}
		},
	)

	// first load the saves
	if load {
		// TODO: check the error
		for _, filename := range options.loadFilenames.argv {
			modelDelta := newModel()
			s := &Save{Version: 1, MarkovModel: modelDelta}
			f, _ := os.Open(filename)
			s.load(f)
			mainModel.join(modelDelta)
		}
	}

	// then read the files and build the model
	if read {
		// TODO: handle the error myself; replace MustCompile
		re := regexp.MustCompile(options.tokenRegex)

		// TODO: make it paraller, maybe
		for _, filename := range options.inputFilenames.argv {
			// TODO: handle the error
			f, _ := os.Open(filename)

			mainModel.join(buildModelFromReader(f, re.Copy(), options.tokensInState))
		}
	}

	// save the model
	if save {
		// TODO: handle the error
		f, _ := os.Create(options.saveFilename)
		s := &Save{Version: 1, MarkovModel: mainModel}
		s.save(f)
	}

	// Generate the output
	if generate && len(mainModel.TMatrix) > 0 {
		// preparations
		rand := xoroshiro.New(options.seed)
		randInt := func() int { return int(rand.Int63()) }

		// actual generation
		current := randInt() % len(mainModel.TMatrix)
		for  i := uint64(0); i < options.outputLength; i++ {
			fmt.Print(mainModel.State[current])

			if mainModel.TMatrix[current].Sum == 0 {
				break
			}

			target := (randInt() % mainModel.TMatrix[current].Sum) + 1
			for id, x := range mainModel.TMatrix[current].Columns {
				target -= x
				if target <= 0 {
					current = id
					break
				}
			}
		}
	}

}

// builds model by feeding it with text slices found by the regex matches
func buildModelFromReader(r io.Reader, re *regexp.Regexp, tokensInState uint) *model {
	m := newModel()

	// TODO: it's dangerous to read the whole file at once.
	fileBytes, _ := ioutil.ReadAll(r)
	fileText := string(fileBytes)
	tokens := re.FindAllString(fileText, -1)

	if len(tokens) == 0 {
		return m
	}

	last := tokens[0]
	for i := 0; i < len(tokens); i++ {
		m.link(last, tokens[i], 1)
		last = tokens[i]
	}

	return m
}
