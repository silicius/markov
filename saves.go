package main

import (
	"encoding/json"
	"io"
)

var saveOptions = struct {
	// encoding
	escapeHTML bool
	prefix     string
	indent     string
	// decoding
	strict bool
}{
	false, "", "\t",
	true,
}

type Save struct {
	Version uint
	//TokensInState uint //maybe
	MarkovModel *model
}

func (s *Save) save(output io.Writer) {
	enc := json.NewEncoder(output)
	enc.SetEscapeHTML(saveOptions.escapeHTML)
	enc.SetIndent(saveOptions.prefix, saveOptions.indent)
	err := enc.Encode(s)
	if err != nil {
		panic(err)
	}
}

func (s *Save) load(input io.Reader) {
	dec := json.NewDecoder(input)
	if saveOptions.strict {
		dec.DisallowUnknownFields()
	}
	err := dec.Decode(s)
	if err != nil {
		panic(err)
	}
}
