# About
`markov` is a program that uses the concept of ([Markov chains](https://en.wikipedia.org/wiki/Markov_chain))
for generating text that looks like its input. The internal state of the markov chain is stored in an object called `model`.

## Installing and running
There are no compiled binaries yet, so you need to download the code and install it.

To download the source code and install it run the following commands:
```
% go get -u gitlab.com/silicius/markov
% go install gitlab.com/silicius/markov
```
The binary should be in the `$GOBIN` folder now.

Example (using markov's own source code):
```
# will generate a save.json in the current folder
% markov -read $GOPATH/src/gitlab.com/silicius/markov/markov.go -splitRegex "\S+[^\S\n]" -save save.json

# generate ouput using the saved model
% markov -load save.json -outputLength 10 -seed 2137
io.Reader, re *regexp.Regexp, tokensInState uint) *model m := s :=
```

## How it works
### Reading file and building the `model`
`markov` accepts a list of files in the `-read` option - each filename separated by comma.

Example:
```
markov -read file1,file2
```
Each file is assumed to be UTF8 text file and is read separately to its own model. At the the end all the models are merged together.

States are extracted from the text with a regex provided with the `-splitRegex` option.
The regex must be of the [re2](https://github.com/google/re2/wiki/Syntax)'s syntax.

Example that catches only non-whitespaces:
```
markov -splitRegex "\S"
```

### Generating the text
The program uses deterministic RNG [xoroshiro128+](http://xoshiro.di.unimi.it/) ported by the [go-xoroshiro](https://github.com/dgryski/go-xoroshiro) package. It means that provided with the same options, the output will be always the same - the opposite of what the default go's `math/rand` package does.
The RNG is supplied with a `int64` seed supplied by the `-seed` option.

The procedure for generating the oputput looks like this:
```
1. Seed the RNG and choose first random firt state from all the states
2. Print the current state to the STDOUT
3. End if any of the following is true:
   3.1. the current state has 0% probality for having any succesor
   3.1. limit of generated states was exceeded
4. Choose the next state based on the probalities in the current state
5 Go to 2
```
So the generating stops if either the `-outputLength` option's vaule has been exceeded or the current state has no succesors to follow. Since the former case is not an error in the program, there will be no message about it - the output will just terminate.

A partial example of generating text would be:
```
markov -seed 1914 -outputLength 100
```

### Saving and loading `model`'s state
with the `-save` and `-load` option you can save the time of generating a `model` from the start each time you start the program.

Saving works by writing the `model` to a specified file in the json format that can be easily read.
Example:
```
markov -save save.json
```

Loading on the other hand can load multiple saved models. At the end all of them are merged to one, preserving all the states and relations between them. The option accepts the same format as `-read`.
Example:
```
markov -load file1,file2
```