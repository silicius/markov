package main

type tMatrixRow struct {
	Sum     int
	Columns []int
}

type model struct {
	State   []string
	TMatrix []*tMatrixRow
}

func newModel() *model {
	return &model{
		make([]string, 0),
		make([]*tMatrixRow, 0),
	}
}

// always returns an ID of given state
func (m *model) get(State string) int {
	for sid, s := range m.State {
		if s == State {
			return sid
		}
	}

	// state not found, make a new one
	id := len(m.State)
	m.State = append(m.State, State)
	// also add to the matrix
	for row := range m.TMatrix {
		m.TMatrix[row].Columns = append(m.TMatrix[row].Columns, 0)
	}
	m.TMatrix = append(m.TMatrix, &tMatrixRow{0, make([]int, id+1)})

	return id
}

func (m *model) link(a string, b string, amount int) {
	aid := m.get(a)
	bid := m.get(b)
	m.TMatrix[aid].Columns[bid] += amount
	m.TMatrix[aid].Sum += amount
}

func (m *model) join(delta *model) {
	for y, row := range delta.TMatrix {
		for x := range row.Columns {
			m.link(delta.State[y], delta.State[x], row.Columns[x])
		}
	}
}
